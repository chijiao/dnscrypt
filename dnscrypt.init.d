#!/bin/sh
### BEGIN INIT INFO
# Provides:          dnscrypt
# Required-Start:    $network $remote_fs $syslog
# Required-Stop:     $network $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start dnscrypt
# Description:       Encrypt DNS queries.
### END INIT INFO
DAEMON="/usr/sbin/dnscrypt-proxy"
NAME="dnscrypt"

dnscrypt_start()
{
    echo "Starting dnscrypt"
    dnscrypt-proxy  --local-address=127.0.0.1 --daemonize    
}

dnscrypt_stop()
{
    echo "Stopping dnscrypt"
    killall $(basename $DAEMON)
}

case "$1" in
    start)
   dnscrypt_start
   ;;
  stop)
   dnscrypt_stop
  ;;
  restart|force-reload)
   dnscrypt_stop
  dnscrypt_start
   ;;
    *)
   echo "Usage: /etc/init.d/$NAME {start|stop|restart|force-reload}" >&2
   exit 1
   ;;
esac

exit 0
