rm -rf /dev/shm/*
mkdir -p /dev/shm/node-root/etc/init.d/
make clean
./configure --prefix=/usr
make
make install DESTDIR=/dev/shm/node-root
cp dnscrypt.init.d /dev/shm/node-root/etc/init.d/dnscrypt
chmod +x /dev/shm/node-root/etc/init.d/dnscrypt
echo "/sbin/chkconfig --add dnscrypt" > /tmp/postinstall
cd /dev/shm/node-root 
fpm -f -s dir -t rpm -n dnscrypt --epoch 1 -v 1.3 \
--iteration 3.el6 -C /dev/shm/node-root \
-p ~/rpmbuild/RPMS/x86_64/ -d 'libsodium' --post-install /tmp/postinstall .
